module.exports = {
  roots: ['<rootDir>/src'],
  collectCoverageFrom: ['<rootDir>qsrc/**/*.ts'],
  coverageDirectory: 'coverage',
  testEnvironment: 'node',
  transform: {
    '.+\\.ts$': 'ts-jest'
  }
}
